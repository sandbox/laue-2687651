<?php

/**
 * @file
 * Contains \Drupal\basic_quiz\QuizHasQuestionInterface.
 */

namespace Drupal\basic_quiz;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining Quiz has question entities.
 *
 * @ingroup quiz
 */
interface QuizHasQuestionInterface extends ContentEntityInterface {

  /**
   * @param \Drupal\basic_quiz\QuizInterface $quiz
   * @return QuizHasQuestionInterface $this
   */
  public function setQuiz(QuizInterface $quiz);

  /**
   * @return QuizInterface $quiz
   */
  public function getQuiz();

  /**
   * @param \Drupal\basic_quiz\QuestionInterface $question
   * @return QuizHasQuestionInterface $this
   */
  public function setQuestion(QuestionInterface $question);

  /**
   * @return QuestionInterface $question
   */
  public function getQuestion();

  /**
   *
   */
  public function getScore();

  /**
   *
   */
  public function setScore($score);

}
