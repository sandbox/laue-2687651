<?php

/**
 * @file
 * Contains \Drupal\basic_quiz\QuizTypeInterface.
 */

namespace Drupal\basic_quiz;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Quiz type entities.
 */
interface QuizTypeInterface extends ConfigEntityInterface {

}
