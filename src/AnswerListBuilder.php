<?php

/**
 * @file
 * Contains \Drupal\basic_quiz\AnswerListBuilder.
 */

namespace Drupal\basic_quiz;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines a class to build a listing of Answer entities.
 *
 * @ingroup answer
 */
class AnswerListBuilder extends EntityListBuilder {
  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Answer ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\basic_quiz\Entity\Answer */
    $row['id'] = $entity->id();
    return $row + parent::buildRow($entity);
  }

}
