<?php

/**
 * @file
 * Contains \Drupal\basic_quiz\AnswerInterface.
 */

namespace Drupal\basic_quiz;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Answer entities.
 *
 * @ingroup answer
 */
interface AnswerInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Returns the question entity for this answer.
   *
   * @return \Drupal\basic_quiz\QuestionInterface
   *   The question entity.
   */
  public function getQuestion();

  /**
   * Sets the entity question.
   *
   * @param \Drupal\basic_quiz\QuestionInterface $question
   *   The question for this answer.
   *
   * @return $this
   */
  public function setQuestion(QuestionInterface $question);

  /**
   * Returns the question entity id for this answer.
   *
   * @return int
   *   Returns the id of question entity.
   */
  public function getQuestionId();

  /**
   *
   */
  public function setState(UserQuizStatusInterface $status);

  /**
   *
   */
  public function getStateId();

  /**
   *
   */
  public function getState();

  /**
   *
   */
  public function isText();

  /**
   *
   */
  public function isTrueFalse();

  /**
   *
   */
  public function isMultipleChoice();

}
