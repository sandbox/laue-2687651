<?php

/**
 * @file
 * Contains \Drupal\basic_quiz\QuestionInterface.
 */

namespace Drupal\basic_quiz;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Question entities.
 *
 * @ingroup question
 */
interface QuestionInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the bundle type of the quiz entity.
   *
   * @return string
   *    Returns the name of the bundle.
   */
  public function getType();

  /**
   * Gets the number of answers a user has given to a question.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *    An user.
   *
   * @return int
   *    Returns the number of answers an user has for this question.
   */
  public function getUserAnswersCount(AccountInterface $account);

  /**
   * Gets all the answers of this question.
   *
   * @return array
   *    Array containing all the answers given to this question.
   */
  public function getAnswers();

  /**
   * Gets the type of answer associated with this question.
   *
   * @return string
   *    A string representing the name of the answer type.
   */
  public function getAnswerType();

  /**
   *
   */
  public function getScore(QuizInterface $quiz);

  /**
   *
   */
  public function setScore(QuizInterface $quiz, $score);

  /**
   *
   */
  public function getDefaultScore();

}
