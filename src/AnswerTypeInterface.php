<?php

/**
 * @file
 * Contains \Drupal\basic_quiz\AnswerTypeInterface.
 */

namespace Drupal\basic_quiz;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Answer type entities.
 */
interface AnswerTypeInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.
}
