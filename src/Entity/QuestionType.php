<?php

/**
 * @file
 * Contains \Drupal\basic_quiz\Entity\QuestionType.
 */

namespace Drupal\basic_quiz\Entity;

use Drupal\basic_quiz\QuestionTypeInterface;
use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Question Type entity.
 *
 * @ConfigEntityType(
 *   id = "question_type",
 *   label = @Translation("Question Type"),
 *   handlers = {
 *     "list_builder" = "Drupal\basic_quiz\QuestionTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\basic_quiz\Form\QuestionTypeForm",
 *       "edit" = "Drupal\basic_quiz\Form\QuestionTypeForm",
 *       "delete" = "Drupal\basic_quiz\Form\QuestionTypeDeleteForm"
 *     }
 *   },
 *   bundle_of = "question",
 *   config_prefix = "question_type",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/question_type/{question_type}",
 *     "edit-form" = "/admin/structure/question_type/{question_type}/edit",
 *     "delete-form" = "/admin/structure/question_type/{question_type}/delete",
 *     "collection" = "/admin/structure/visibility_group"
 *   }
 * )
 */
class QuestionType extends ConfigEntityBundleBase implements QuestionTypeInterface {
  /**
   * The Question Type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Question Type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Answer type uuid.
   *
   * @var string
   */
  protected $uuid;

  /**
   * @inheritdoc
   */
  public function isTrueFalse() {
    return ($this->id() == 'true_or_false');
  }

  /**
   * @inheritdoc
   */
  public function isText() {
    return ($this->id() == 'text_question');
  }

  /**
   * @inheritdoc
   */
  public function isMultipleChoice() {
    return ($this->id() == 'multiple_choice_question');
  }

}
