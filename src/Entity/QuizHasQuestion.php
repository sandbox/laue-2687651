<?php

/**
 * @file
 * Contains \Drupal\basic_quiz\Entity\QuizHasQuestion.
 */

namespace Drupal\basic_quiz\Entity;

use Drupal\basic_quiz\QuestionInterface;
use Drupal\basic_quiz\QuizHasQuestionInterface;
use Drupal\basic_quiz\QuizInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the Quiz has question entity.
 *
 * @ingroup quiz
 *
 * @ContentEntityType(
 *   id = "quiz_has_question",
 *   label = @Translation("Quiz has question"),
 *   base_table = "quiz_has_question",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   },
 * )
 */
class QuizHasQuestion extends ContentEntityBase implements QuizHasQuestionInterface {
  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
  }

  /**
   * {@inheritdoc}
   */
  public function setQuiz(QuizInterface $quiz) {
    $this->set('quiz', $quiz->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuiz() {
    return $this->get('quiz')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setQuestion(QuestionInterface $question) {
    $this->set('question', $question->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->get('question')->entity;
  }

  /**
   *
   */
  public function getScore() {
    return $this->get('score')->value;
  }

  /**
   *
   */
  public function setScore($score) {
    $this->set('score', $score);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Quiz has question entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Quiz has question entity.'))
      ->setReadOnly(TRUE);

    $fields['quiz'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Quiz'))
      ->setDescription(t('The quiz this relation references'))
      ->setSetting('target_type', 'quiz');

    $fields['question'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Question'))
      ->setDescription(t('The question this relation references'))
      ->setSetting('target_type', 'question');

    $fields['score'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Score'))
      ->setDescription(t('This the score this question has for this quiz'));

    return $fields;
  }

}
