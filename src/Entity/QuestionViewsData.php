<?php

/**
 * @file
 * Contains \Drupal\basic_quiz\Entity\question.
 */

namespace Drupal\basic_quiz\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Question entities.
 */
class QuestionViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['question']['table']['base'] = [
      'field' => 'id',
      'title' => $this->t('Question'),
      'help' => $this->t('The Question ID.'),
    ];

    return $data;
  }

}
