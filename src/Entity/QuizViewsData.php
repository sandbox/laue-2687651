<?php

/**
 * @file
 * Contains \Drupal\basic_quiz\Entity\Quiz.
 */

namespace Drupal\basic_quiz\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Quiz entities.
 */
class QuizViewsData extends EntityViewsData implements EntityViewsDataInterface {
  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['quiz']['table']['base'] = [
      'field' => 'id',
      'title' => $this->t('Quiz'),
      'help' => $this->t('The Quiz ID.'),
    ];

    return $data;
  }

}
