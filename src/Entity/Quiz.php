<?php

/**
 * @file
 * Contains \Drupal\basic_quiz\Entity\Quiz.
 */

namespace Drupal\basic_quiz\Entity;

use Drupal\basic_quiz\QuestionInterface;
use Drupal\basic_quiz\QuizInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;

// "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",.
/**
 * Defines the Quiz entity.
 *
 * @ingroup quiz
 *
 * @ContentEntityType(
 *   id = "quiz",
 *   label = @Translation("Quiz"),
 *   bundle_label = @Translation("Quiz bundle"),
 *   handlers = {
 *     "view_builder" = "Drupal\basic_quiz\QuizViewBuilder",
 *     "list_builder" = "Drupal\basic_quiz\QuizListBuilder",
 *     "views_data" = "Drupal\basic_quiz\Entity\QuizViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\basic_quiz\Entity\Form\QuizForm",
 *       "add" = "Drupal\basic_quiz\Entity\Form\QuizForm",
 *       "edit" = "Drupal\basic_quiz\Entity\Form\QuizForm",
 *       "delete" = "Drupal\basic_quiz\Entity\Form\QuizDeleteForm",
 *     },
 *     "access" = "Drupal\basic_quiz\QuizAccessControlHandler",
 *   },
 *   base_table = "quiz",
 *   admin_permission = "administer Quiz entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "bundle" = "type"
 *   },
 *   bundle_entity_type = "quiz_type",
 *   links = {
 *     "canonical" = "/quiz/{quiz}",
 *     "edit-form" = "/admin/quiz/{quiz}/edit",
 *     "delete-form" = "/admin/quiz/{quiz}/delete"
 *   },
 *   field_ui_base_route = "entity.quiz_type.edit_form"
 * )
 */
class Quiz extends ContentEntityBase implements QuizInterface {
  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }


  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestions() {
    /* @var $questionRelation \Drupal\basic_quiz\Entity\QuizHasQuestion */
    $quizHasQuestionStorage = static::entityTypeManager()
      ->getStorage('quiz_has_question');
    $query = $quizHasQuestionStorage->getQuery();
    $quizHasQuestionIds = $query
      ->Condition('quiz', $this->id())
      ->execute();

    $quizHasQuestions = $quizHasQuestionStorage->loadMultiple($quizHasQuestionIds);
    $questions = [];
    /* @var $quizHasQuestion \Drupal\basic_quiz\Entity\QuizHasQuestion */
    foreach ($quizHasQuestions as $quizHasQuestion) {
      $questions[] = $quizHasQuestion->getQuestion();
    }
    return $questions;
  }

  /**
   * Select q.id from question as q
   * left join quiz_has_question as h
   * on h.question = q.id
   * where h.question is null.
   */
  public function getUnselectedQuestions() {
    $questionStorage = static::entityTypeManager()->getStorage('question');
    $connection = \Drupal::database();
    $query = $connection->select('question', 'q')->fields('q', ['id']);
    $query->addJoin('LEFT', 'quiz_has_question', 'h', 'q.id = h.question');
    $query->isNull('h.question');
    $result = $query->execute();
    $rows = $result->fetchCol(0);
    $questions = $questionStorage->loadMultiple($rows);
    return ($questions);
  }

  /**
   *
   */
  public function getAllQuestions() {
    /* @var $questionRelation \Drupal\basic_quiz\Entity\QuizHasQuestion */
    $questionStorage = static::entityTypeManager()->getStorage('question');
    $query = $questionStorage->getQuery();
    $questionIds = $query->execute();
    return $questionStorage->loadMultiple($questionIds);
  }

  /**
   * {@inheritdoc}
   */
  public function getStatuses(AccountInterface $user = NULL) {
    $statusStorage = static::entityTypeManager()
      ->getStorage('user_quiz_status');
    $query = $statusStorage->getQuery();
    if ($user != NULL) {
      $qidList = $query
        ->condition('quiz', $this->id())
        ->condition('user_id', $user->id())
        ->execute();
    }
    else {
      $qidList = $query
        ->condition('quiz', $this->id())
        ->execute();
    }
    return $statusStorage->loadMultiple($qidList);
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveStatus(AccountInterface $user) {
    $statusStorage = static::entityTypeManager()
      ->getStorage('user_quiz_status');
    $query = $statusStorage->getQuery();
    $qidList = $query
      ->condition('quiz', $this->id())
      ->condition('user_id', $user->id())
      ->condition('finished', 0)
      ->execute();
    // kint($qidList);
    if (!empty($qidList)) {
      return $statusStorage->load(key($qidList));
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getMaxScore() {
    $questions = $this->getQuestions();
    $score = 0;
    foreach ($questions as $question) {
      /* @var $question \Drupal\basic_quiz\Entity\Question */
      $score += $this->getQuestionScoreById($question->id());
    }
    return $score;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestionCount() {
    $storage = static::entityTypeManager()->getStorage('quiz_has_question');
    $query = $storage->getQuery();
    $questionCount = $query->Condition('quiz', $this->id())->count()->execute();
    return $questionCount;
  }

  /**
   * {@inheritdoc}
   */
  public function removeQuestion(QuestionInterface $question) {
    $storage = static::entityTypeManager()->getStorage('quiz_has_question');
    $query = $storage->getQuery();
    $qids = $query
      ->Condition('quiz', $this->id())
      ->Condition('question', $question->id())
      ->execute();
    $relations = $storage->loadMultiple($qids);
    foreach ($relations as $relation) {
      $relation->delete();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getPercentile() {
    return $this->get('percent')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getTimeLimit() {
    return $this->get('time')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getAttemptLimit() {
    return $this->get('attempts')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   *
   */
  public function getQuestionScoreById($qid) {
    $storage = static::entityTypeManager()->getStorage('quiz_has_question');
    $query = $storage->getQuery();
    $qids = $query
      ->Condition('quiz', $this->id())
      ->Condition('question', $qid)
      ->execute();
    $relation = $storage->load(current($qids));
    return $relation->getScore();
  }

  /**
   *
   */
  public function setQuestionScoreById($qid, $score) {
    $storage = static::entityTypeManager()->getStorage('quiz_has_question');
    $query = $storage->getQuery();
    $qids = $query
      ->Condition('quiz', $this->id())
      ->Condition('question', $qid)
      ->execute();
    $relations = $storage->loadMultiple($qids);
    foreach ($relations as $relation) {
      $relation->setScore($score);
      $relation->save();
    }
    return $this;
  }

  /**
   *
   */
  public function removeQuestionById($qid) {
    $storage = static::entityTypeManager()->getStorage('quiz_has_question');
    $query = $storage->getQuery();
    $qids = $query
      ->Condition('quiz', $this->id())
      ->Condition('question', $qid)
      ->execute();
    $relations = $storage->loadMultiple($qids);
    foreach ($relations as $relation) {
      $relation->delete();
    }
    return $this;
  }

  /**
   *
   */
  public function addQuestionById($qid) {
    $storage = static::entityTypeManager()->getStorage('question');
    /* @var $question \Drupal\basic_quiz\Entity\Question */
    $question = $storage->load($qid);
    $entity = QuizHasQuestion::create([])
      ->setQuestion($question)
      ->setQuiz($this)
      ->setScore($question->getDefaultScore())
      ->save();

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Quiz entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Quiz entity.'))
      ->setReadOnly(TRUE);

    $fields['type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Type'))
      ->setDescription(t('The quiz type.'))
      ->setSetting('target_type', 'quiz_type')
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Quiz entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Quiz entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('The description of this quiz'))
      ->setSettings([
        'max_length' => 256,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['percent'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Pass rate'))
      ->setDescription(t('The pass rate for this quiz in percents.'))
      ->setDefaultValue(50)
      ->addPropertyConstraints('value', ['Range' => ['min' => 0, 'max' => 100]])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ]);

    $fields['time'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Quiz time (seconds)'))
      ->setDescription(t('The number of seconds the user has to complete the quiz after starting it. Set to 0 for no time limit.'))
      ->setDefaultValue(0)
      ->addPropertyConstraints('value', ['Range' => ['min' => 0]])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ]);

    $fields['attempts'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Number of attempts allowed'))
      ->setDescription(t('The number a time an user is allowed to attempt this quiz. Set to 0 for unlimited attempts.'))
      ->setDefaultValue(0)
      ->addPropertyConstraints('value', ['Range' => ['min' => 0]])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ]);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code for the Quiz entity.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
