<?php

/**
 * @file
 * Contains \Drupal\basic_quiz\Entity\Answer.
 */

namespace Drupal\basic_quiz\Entity;

use Drupal\basic_quiz\AnswerInterface;
use Drupal\basic_quiz\QuestionInterface;
use Drupal\basic_quiz\UserQuizStatusInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;

/**
 * Defines the Answer entity.
 *
 * @ingroup answer
 *
 * @ContentEntityType(
 *   id = "answer",
 *   label = @Translation("Answer"),
 *   bundle_label = @Translation("Answer bundle"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\basic_quiz\AnswerListBuilder",
 *     "views_data" = "Drupal\basic_quiz\Entity\AnswerViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\basic_quiz\Entity\Form\AnswerForm",
 *       "add" = "Drupal\basic_quiz\Entity\Form\AnswerForm",
 *       "edit" = "Drupal\basic_quiz\Entity\Form\AnswerForm",
 *       "delete" = "Drupal\basic_quiz\Entity\Form\AnswerDeleteForm",
 *     },
 *     "access" = "Drupal\basic_quiz\AnswerAccessControlHandler",
 *   },
 *   base_table = "answer",
 *   admin_permission = "administer Answer entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "bundle" = "type"
 *   },
 *   bundle_entity_type = "answer_type",
 *   links = {
 *     "canonical" = "/admin/answer/{answer}",
 *     "edit-form" = "/admin/answer/{answer}/edit",
 *     "delete-form" = "/admin/answer/{answer}/delete"
 *   },
 *   field_ui_base_route = "entity.answer_type.edit_form"
 * )
 */
class Answer extends ContentEntityBase implements AnswerInterface {
  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->get('question')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestionId() {
    return $this->get('question')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setQuestion(QuestionInterface $question) {
    $this->set('question', $question->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle();
  }

  /**
   *
   */
  public function setState(UserQuizStatusInterface $status) {
    $this->set('user_quiz_status', $status);
    return $this;
  }

  /**
   *
   */
  public function getStateId() {
    return $this->get('user_quiz_status')->target_id;
  }

  /**
   *
   */
  public function getState() {
    return $this->get('user_quiz_status')->entity;
  }

  /**
   *
   */
  public function isText() {
    return ($this->bundle() == 'text_answer');
  }

  /**
   *
   */
  public function isTrueFalse() {
    return ($this->bundle() == 'true_or_false');
  }

  /**
   *
   */
  public function isMultipleChoice() {
    return ($this->bundle() == 'multiple_choice_answer');
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Answer entity.'))
      ->setReadOnly(TRUE);

    $fields['question'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Question'))
      ->setSetting('target_type', 'question')
      ->setDisplayConfigurable('view', TRUE)
      ->setReadOnly(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ]);

    $fields['user_quiz_status'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User-Quiz status'))
      ->setSetting('target_type', 'user_quiz_status');

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Answer entity.'))
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Answer entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Type'))
      ->setDescription(t('The answer type.'))
      ->setSetting('target_type', 'answer_type')
      ->setReadOnly(TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code for the Answer entity.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
