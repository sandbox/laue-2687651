<?php

/**
 * @file
 * Contains \Drupal\basic_quiz\QuizSelectedQuestions.
 */

namespace Drupal\basic_quiz\Form;

use Drupal\basic_quiz\QuestionInterface;
use Drupal\basic_quiz\QuizInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list of selected questions for a quiz.
 */
class QuizTakeAllForm extends FormBase {

  /**
   * The current quiz.
   *
   * @var \Drupal\basic_quiz\Entity\Quiz
   */
  protected $quiz;

  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('quiz')
    );
  }

  /**
   * QuizSelectedQuestionsForm constructor.
   *
   * @param \Drupal\basic_quiz\QuizInterface $quiz
   */
  public function __construct(QuizInterface $quiz) {
    $this->quiz = $quiz;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'quiz_take_all';
  }

  /**
   *
   */
  public function buildAnswer(QuestionInterface $question) {

    /* @var $answer \Drupal\basic_quiz\Entity\Answer */
    $answer = static::entityTypeManager()->getStorage('answer')->create([
      'type' => $question->getAnswerType(),
      'question' => $question->id(),
      'user_quiz_status' => 1,
    ]);

    /* @var $formBuilder \Drupal\Core\Entity\EntityFormBuilder */
    $formBuilder = \Drupal::service('entity.form_builder');
    $formBuilder->getForm($answer);
    $form['question'] = [
      '#type' => 'label',
      '#title' => $question->label(),
      '#weight' => -5,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $questionRelation \Drupal\basic_quiz\Entity\QuizHasQuestion */
    /* @var $question \Drupal\basic_quiz\Entity\Question */
    $questions = $this->quiz->getQuestions();

    $form['question'] = [];
    /*
    foreach ($questions as $question) {
    $form['question'][$question->id()] = $this->buildAnswer($question);
    }
     */

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Remove Selected / Update Score'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    return;
  }

  /**
   * Gets the manager.
   * "I demand to speak with the Manager!".
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  protected function entityTypeManager() {
    if (!isset($this->entityTypeManager)) {
      $this->entityTypeManager = $this->container()->get('entity_type.manager');
    }
    return $this->entityTypeManager;
  }

  /**
   * Returns the service container.
   *
   * This method is marked private to prevent sub-classes from retrieving
   * services from the container through it. Instead,
   * \Drupal\Core\DependencyInjection\ContainerInjectionInterface should be used
   * for injecting services.
   *
   * @return \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   */
  private function container() {
    return \Drupal::getContainer();
  }

}
