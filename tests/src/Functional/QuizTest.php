<?php
/**
 * @file
 * Contains \Drupal\Tests\quiz\Functional\QuizTest.
 */

namespace Drupal\Tests\quiz\Functional;

use Drupal\simpletest\WebTestBase;

/**
 * Functional test for creating Quizzes.
 *
 * @ingroup quiz
 *
 * @group quiz
 */
class QuizTest extends WebTestBase {

  /**
   * @var \Drupal\user\Entity\User.
   */
  protected $user;

  /**
   *
   */
  function setUp() {
    parent::setUp();
    $this->user = $this->drupalCreateUser('add quiz entities');
  }

  /**
   *
   */
  public function testCreateQuiz() {
    $this->drupalLogin($this->user);
    $this->drupalGet('admin/quiz/add');
    // Set up some content.
    /*
    $settings = array(
    'type' => 'simpletest_example',
    'title' => $this->randomMachineName(32),
    );
    // Create the content node.
    $node = $this->drupalCreateNode($settings);
    // View the node.
    $this->drupalGet('node/' . $node->id());
    // Check that our module did it's thing.
    $this->assertText(t('The test module did its thing.'), "Found evidence of test module.");
     */
  }

}
